var gulp = require('gulp');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');  // usado para executar tasks em ordem

gulp.task('default', ['build']);

gulp.task('build', function() {
    runSequence('clean', ['minify', 'sass']);
})

gulp.task('clean', function() {
    return gulp.src('dist', { read: false })
      .pipe(clean());
});

gulp.task('minify', function() {
  return gulp.src('./js/*.js')
    .pipe(uglify())
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass', function() {
   return gulp.src('./sass/**/*.scss')
    .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(concat('app.css'))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./sass/**/*.scss', ['sass']);
});

// Observações:
// if scripts order matters use an array in gulp.src function
//     example: gulp.src(['a.js', 'b.js'])
// a task sass:watch quando executada fica observando os arquivos sass e recompila automaticamente depois de alterados
// caso alguma task dependa de outra para ser executada usar a seguinte sintaxe gulp.task('minify', ['clean'], function() {
